import socket

MCAST_GRP = '239.2.2.2'
MCAST_PORT = 5007

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
sock.sendto("", (MCAST_GRP, MCAST_PORT))
sock.settimeout(5)
servers = []
while True:
	try:
		data,address = sock.recvfrom(10240)
		addr = data.replace('127.0.1.1', address[0])
		servers.append(addr)
	except socket.timeout:
		break

print(servers)

