# Frogs and Flies Server
# Server logic by Rasmus
import Pyro.core
from game import Game
from servermanager import ServerManager
import threading

def run(servermanager, height, width):
	Pyro.core.initServer()
	daemon=Pyro.core.Daemon()

	game = Game(daemon, height, width)
	uri = daemon.connect(game, "game")
	servermanager.set_uri(str(uri))
	server_manager_thread = threading.Thread(target=broadcast, name="Server Manager Thread", args=(servermanager,height,width,))
	server_manager_thread.start()

	daemon.requestLoop()

def broadcast(sm, h, w):
	uri = sm.get_uri()
	sm.broadcast_server(h, w)

