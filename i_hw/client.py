import sys
import os
import time
import threading
import cPickle
import client_gui as cgui
import Tkinter as tk
from subprocess import call

import Pyro.core
from Pyro.errors import PyroError
from player import Player

board = None
board_data = '' # board data is placed here
turbo_jump = False # if player wants to move two spaces
game = None
player = None

def read_data():
	if len(sys.argv) < 1:
		sys.stderr.write('Usage: client.py\n')
		sys.exit(1)
	#return sys.argv[1]

def board_loop(rows, columns, root, uri):
	board = cgui.GameBoard(root, rows, columns, uri)
	board.pack()
	root.mainloop()
	
def run(uri, height, width):
	root = tk.Toplevel()
	board_loop(int(height), int(width), root, uri)
