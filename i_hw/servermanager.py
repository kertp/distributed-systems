from socket import * 
from datetime import datetime
import threading
import struct
import collections
import time

MCAST_GRP = '239.2.2.2'
BCAST_PORT = 5007

class ServerManager:

	def __init__(self):
		self.servers = collections.OrderedDict()
		self.lock = threading.RLock()
		self.uri_lock = threading.RLock()
		self.uri = None

	def get_uri(self):
		with self.uri_lock:
			return self.uri

	def set_uri(self, uri):
		with self.uri_lock:
			self.uri = uri

	def add_server(self, server_data, address):
		data = server_data.split("!")
		with self.lock:
			self.servers[address] = Address(address, data[0], data[1], data[2]) # data = URI, height, width

	def remove_server(self, owner):
		with self.lock:
			self.servers.pop(owner)

	def get_servers(self):
		with self.lock:
			return self.servers

	def fetch_servers(self):
		s = threading.Thread(target=validate_servers, name="Validation thread", args=(self,))
		s.start()

		server_sock = socket(AF_INET, SOCK_DGRAM)
		server_sock.bind(('0.0.0.0', BCAST_PORT))
		
		while 1:
			data,addr = server_sock.recvfrom(10240)
			print data
			server_data = data.replace('127.0.0.1', addr[0])
			self.add_server(server_data, addr)
					
		
	def broadcast_server(self, height, width):	
		print("Broadcasting address" + self.uri)		
		s = socket(AF_INET, SOCK_DGRAM)
		s.bind(('',0))
		s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
		
		msg = self.uri + "!" + str(height) + "!" + str(width)
		
		while True:
			s.sendto(msg, ('<broadcast>', BCAST_PORT))
			time.sleep(5)

def validate_servers(sm):
	while(True):
		to_remove = []
		for server in sm.servers:
			if(sm.servers[server].is_dead()): # URI
				to_remove.append(server)

		for i in to_remove:
			sm.remove_server(i)

		time.sleep(3)

class Address:

	def __init__(self, address, uri, height, width):
		self.address = address
		self.uri = uri
		self.height = height
		self.width = width
		self.lastheard = datetime.now()
		self.time_lock = threading.RLock()
		self.timeout   = 10

	def is_dead(self):
		current_time = datetime.now()
		last_heard   = self.get_last_heard()
		diff 		 = current_time - last_heard
		return(abs(diff.seconds) > 10)

	def get_last_heard(self):
		with self.time_lock:
			return self.lastheard
