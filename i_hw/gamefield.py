from player import Player
import random
import time
import threading

class GameField:

	UP = "UP"
	LEFT = "LEFT"
	RIGHT = "RIGHT"
	DOWN = "DOWN"

	def __init__(self, m, n):
		self.game_board = []
		self.players = {}
		self.moves = {}
		self.spectators = []
		self.n = n
		self.m = m
		self.dummy_player = Player("DUMMY", Player.DUMMY)
		self.initialize_array(m, n)

		self.lock = threading.RLock()
		self.move_lock = threading.RLock()
		self.player_lock = threading.RLock()
		self.game_board_lock = threading.RLock()
		self.spectator_lock = threading.RLock()

	def initialize_array(self, m, n):
		internal_array = []

		for i in range(n):
			internal_array.append(self.dummy_player)

		for i in range(m):
			self.game_board.append(internal_array[:])

	def append_player(self, player):
		with self.player_lock:
			self.players[player.uuid] = player

	def get_player(self, uuid):
		with self.player_lock:
			try:
				return self.players[uuid]
			except keyError:
				return False

	def get_all_players(self):
		with self.player_lock:
			return self.players

	def set_on_game_board(self, x,y, player):
		with self.game_board_lock:
			self.game_board[y][x] = player

	def get_from_game_board(self, x, y):
		with self.game_board_lock:
			return self.game_board[y][x]

	def append_spectators(self, player):
		with self.spectator_lock:
			self.spectators.append(player)

	def clear_spectators(self):
		with self.spectator_lock:
			self.spectators = []

	def request_move(self, uuid, direction, times = 1):
		with self.move_lock:
			self.moves[uuid] = (direction, times)

	def get_moves(self):
		with self.move_lock:
			return self.moves

	def get_size(self):
		return (self.m, self.n)