import os
import time
from Tkinter import *
import server as srv
import client as cl
import threading
import subprocess
from servermanager import ServerManager

root = Tk()
main_panel_dead = False
sm = ServerManager()

def kill_main_panel():
	global main_panel_dead
	main_panel_dead = True
	root.destroy()
	
def run_client(height, width):
	uri = None
	while uri == None:
		uri = sm.get_uri()
	cl.run(uri,height, width)

def start_server(height, width):
	srv.run(sm, height, width)
	
def server_selected(event):
	print(event)
	widget = event.widget
	selection = widget.curselection()
	value = widget.get(selection[0])
	servers = sm.get_servers()
	uri = servers[value].uri
	height = servers[value].height
	width = servers[value].width
	srv_list.destroy()
	cl.run(uri, height, width)

	main_panel()

def run_listbox():

	kill_main_panel()
	global srv_list
	srv_list = Tk()

	global listbox
	threading.Thread(target=listen, name="Client thread", args=()).start()

	scrollbar = Scrollbar(srv_list, orient="vertical")
	listbox = Listbox(srv_list, width=80, height=30, yscrollcommand=scrollbar.set)
	listbox.bind("<Double-Button-1>", server_selected)
	listbox.pack()
	listbox.insert(END, "AVAILABLE SERVERS")

	fetch_servers()
	srv_list.mainloop()

def fetch_servers():
	srv_list.after(3000, fetch_servers)

	servers = sm.get_servers()
	listbox.delete(1,END)
	listbox.update_idletasks()
	for srv in servers:
		listbox.insert(END, servers[srv].address)
		listbox.update_idletasks()


def run_server():
	height = ""
	width = ""
	
	while (not height.isdigit() or int(height) < 5 or int(height) > 15):
		height = raw_input("Enter field height (5...15): ")
	while (not width.isdigit() or int(width) < 5 or int(width) > 15):
		width = raw_input("Enter field width (5...15): ")
	kill_main_panel()
	s = threading.Thread(target=start_server, name="Server thread", args=(int(height), int(width), ))
	s.start()
	c = threading.Thread(target=run_client, name="Client thread", args=(int(height), int(width), ))
	c.start()

	c.join()
	s.join()

	main_panel()
	
def main_panel():
	global main_panel_dead
	global root
	
	if (main_panel_dead):
		root = Tk()
	b_cl = Button(root, text="CLIENT", command=run_listbox) # if user starts client, start the listbox first to select a server
	b_srv = Button(root, text="SERVER", command=run_server)
	b_cl.pack()
	b_srv.pack()

	main_panel_dead = False
	root.mainloop()

def listen():
	sm.fetch_servers()
	
if __name__ == '__main__':
	main_panel()