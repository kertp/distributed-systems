import Pyro.core

from datetime import datetime
import uuid
import threading

class Player(Pyro.core.ObjBase):

	DUMMY = 0
	SPECTATOR = 1
	FROG = 2
	FLY = 3

	def __init__(self, name, player_type = SPECTATOR, game = None):
		Pyro.core.ObjBase.__init__(self)
		self.game = game
		self.name = name
		self.player_type = player_type
		self.uuid = str(uuid.uuid4())
		self.last_scored = datetime.now()
		self.joined = datetime.now()
		self.points = 0
		self.current_x = -1
		self.current_y = -1
		
		self.point_lock = threading.RLock()
		self.player_type_lock = threading.RLock()
		self.location_lock = threading.RLock()
		self.time_lock = threading.RLock()

	def move(self, direction, times = 1):
		self.game.request_move(self.uuid, direction, times)

	def is_dummy(self):
		with self.player_type_lock:
			return self.player_type == self.DUMMY

	def is_spectator(self):
		with self.player_type_lock:
			return self.player_type == self.SPECTATOR

	def is_fly(self):
		with self.player_type_lock:
			return self.player_type == self.FLY

	def is_frog(self):
		with self.player_type_lock:
			return self.player_type == self.FROG

	def set_player_type(self, player_type):
		with self.player_type_lock:
			self.player_type = player_type
			self.update_last_scored()

	def get_location(self):
		with self.location_lock:
			return (self.current_x, self.current_y)

	def set_location(self, x, y):
		with self.location_lock:
			self.current_x = x
			self.current_y = y

	def get_points(self):
		with self.point_lock:
			return self.points

	def set_points(self, points):
		with self.point_lock:
			self.points = points

	def get_last_scored(self):
		with self.time_lock:
			return self.last_scored

	def inc_points(self):
		current_points = self.points
		self.set_points(current_points+1)

	def exit_game(self):
		self.game.exit(self)

	def last_action_limit_exceeded(self):
		current_time = datetime.now()
		diff = current_time - self.get_last_scored()
		return not self.is_spectator() and abs(diff.seconds) >= 120

	def kill(self):
		self.set_location(-1,-1)
		self.set_points(0)
		self.set_player_type(self.SPECTATOR)

	def get_visibility_area(self):
		return self.game.get_visibility_area(self)

	def update_last_scored(self):
		with self.time_lock:
			self.last_scored = datetime.now()

	def get_icon(self):
		if self.is_frog():
			return "R"
		elif self.is_fly():
			return "F"
		else:
			return "_"

	def change_role(self, requested_type):
		self.game.change_role(requested_type, self)

	def get_name(self):
		return self.name

	def __str__(self):
		return "[UUID:" + str(self.uuid) + " name:" + str(self.name) + " type:" + str(self.player_type) + "]"
