import Tkinter as tk
import tkMessageBox
import tkSimpleDialog
import client as cl
import threading
import time
from player import Player
import sys

import Pyro.core
from Pyro.errors import PyroError
from player import Player

UP = "UP"
LEFT = "LEFT"
RIGHT = "RIGHT"
DOWN = "DOWN"

class GameBoard(tk.Frame):
	def __init__(self, parent, rows, columns, uri, size=64, color1="white", color2="black"):	

		self.parent = parent
		self.rows = rows
		self.columns = columns
		self.size = size
		self.color1 = color1
		self.color2 = color2
		self.pieces = {}
		self.last_command = ""
		self.turbo = False
		self.is_frog = False
		self.score = 0
		self.icons = []
		self.fly = tk.PhotoImage(file = "House-Fly-icon.gif")
		self.frog = tk.PhotoImage(file = "frog-icon.gif")
		self.player = None
		self.game = None
		self.name = None
		self.connected = False
		self.registered = False
		self.main_thread = None
		self.exit_requested = False

		#self.parent.withdraw()
		self.connect(uri)
		self.register()
		#self.parent.deiconify()
		
		canvas_width = columns * size
		canvas_height = rows * size

		tk.Frame.__init__(self, parent)
		self.canvas = tk.Canvas(self, borderwidth=0, highlightthickness=0, width=canvas_width, height=canvas_height, background="bisque")
		self.canvas.bind("<Key>", self.key)
		self.canvas.bind("<Button-1>", self.callback)
		self.canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2) 	

		self.canvasRegisterButton = tk.Canvas(self, borderwidth=0, highlightthickness=0, width=80, height=80, background="white")
		self.canvasRegisterButton.bind("<Button-1>", self.exit)
		self.canvasRegisterButton.create_text(40, 40, text="EXIT")
		self.canvasRegisterButton.pack(side="right")

		self.main_thread = threading.Thread(target=self.main_loop, name="Main", args=())
		self.main_thread.start()
		
	def key(self, event):
		try:
			key = event.char
			if key == "1": # "1"
				self.player.change_role(Player.FLY)
				self.is_frog = False
			elif key == "2": # "2"
				self.player.change_role(Player.FROG)
				self.is_frog = True
			elif key == "3": # "3"
				self.player.change_role(Player.SPECTATOR)
			elif self.turbo and self.is_frog and key == "w":
				self.player.move(UP, 2)
			elif self.turbo and self.is_frog and key == "s":
				self.player.move(DOWN, 2)
			elif self.turbo and self.is_frog and key == "a":
				self.player.move(LEFT, 2)
			elif self.turbo and self.is_frog and key == "d":
				self.player.move(RIGHT, 2)
			elif key == "w":
				self.player.move(UP)
			elif key == "s":
				self.player.move(DOWN)
			elif key == "a":
				self.player.move(LEFT)
			elif key == "d":
				self.player.move(RIGHT)
			elif key == " ":
				self.turbo = not self.turbo
		except:
			print("Connection lost....")	
			self.parent.destroy()

	def exit(self, event):
		try:
			if(self.player != None):
				self.player.exit_game()
			self.parent.destroy()
		except:
			print("Connection lost....")	
			self.parent.destroy()
			
	def callback(self, event):
		self.canvas.focus_set()
				
	def register(self):
		try:
			if(self.connected):
				self.registered = True
				self.name = tkSimpleDialog.askstring("Enter your name", "Name")
				player_uri = self.game.register(self.name)
				self.player = Pyro.core.getProxyForURI(player_uri)

				correct = False
				
				while not correct:
					role = tkSimpleDialog.askstring("Enter your role: FLY, FROG or SPEC", "Role: FLY, FROG or SPEC")
					if role.lower() == "FLY".lower():
						self.player.change_role(Player.FLY)
						self.is_frog = False
						correct = True
					elif role.lower() == "FROG".lower():
						self.player.change_role(Player.FROG)
						self.is_frog = True
						correct = True
					elif role.lower() == "SPEC".lower():
						self.player.change_role(Player.SPECTATOR)
					else:
						print "Invalid role. Try again."
		except:
			print("Connection lost....")	
			self.parent.destroy()
	
	def connect(self, uri):
		self.game = Pyro.core.getProxyForURI(uri)
		self.connected = True
		
	def main_loop(self):
		try:
			while 1:
				board_data = self.player.get_visibility_area()
				score = self.player.get_points()
				print('Your score is', score)
				self.refresh(board_data)
				time.sleep(0.35)
		except:
			print("Connection lost....")	
			self.parent.destroy()
		
	def addCharacter(self, row, column, type):
		name = type + str(row) + str(column)
		if type == 'F':
			self.canvas.create_image(0,0, image=self.fly, tags=(name, "fly"), anchor="c")
		elif type == 'R':
			self.canvas.create_image(0,0, image=self.frog, tags=(name, "frog"), anchor="c")
		
		self.icons.append(name)
		x0 = (column * self.size) + int(self.size/2)
		y0 = (row * self.size) + int(self.size/2)
		self.canvas.coords(name, x0, y0)

	def refresh(self, data):
		self.canvas.delete("square")
		for icon in self.icons:
			self.canvas.delete(icon)
		self.icons = []
		color = self.color2
		for row in range(self.rows):
			for col in range(self.columns):
				if data[row][col] == '_':
					x1 = (col * self.size)
					y1 = (row * self.size)
					x2 = x1 + self.size
					y2 = y1 + self.size
					self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill="white", tags="square")
				elif data[row][col] == '#':
					x1 = (col * self.size)
					y1 = (row * self.size)
					x2 = x1 + self.size
					y2 = y1 + self.size
					self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill="black", tags="square")
				elif data[row][col] == 'F': # fly
					x1 = (col * self.size)
					y1 = (row * self.size)
					x2 = x1 + self.size
					y2 = y1 + self.size
					self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill="white", tags="square")
					self.addCharacter(row, col, 'F')
				elif data[row][col] == 'R': # frog
					x1 = (col * self.size)
					y1 = (row * self.size)
					x2 = x1 + self.size
					y2 = y1 + self.size
					self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill="white", tags="square")
					self.addCharacter(row, col, 'R')

		self.canvas.tag_raise("piece")
		self.canvas.tag_lower("square")
		self.update_idletasks()
		
		