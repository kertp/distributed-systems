import Pyro.core
from player import Player
from engine import Engine
from gamefield import GameField
import threading

#HEIGHT = 10
#WIDTH = 10

class Game(Pyro.core.ObjBase):

	def __init__(self, daemon, height, width):
		Pyro.core.ObjBase.__init__(self)
		self.game_field = GameField(width, height)
		self.height = height
		self.width = width
		self.engine = Engine()
		t = threading.Thread(target=self.engine.run, args=(self.game_field,))
		t.start()

	def register(self, name):
		player = Player(name, Player.SPECTATOR, self)
		self.engine.add_player(self.game_field, player)
		u_uri = self.daemon.connect(player,name)
		return u_uri

	def request_move(self, uuid, direction, times = 1):
		return self.engine.request_move(self.game_field, uuid, direction, times)

	def get_visibility_area(self, player):
		visibility_area = self.engine.get_visible_area(self.game_field, player)
		return visibility_area

	def change_role(self, player, new_role):
		self.engine.change_player_role(self.game_field, player, new_role)

	def exit(self, player):
		self.engine.remove_player_from_board(self.game_field, player)

	def get_size(self):
		return (self.width, self.height)