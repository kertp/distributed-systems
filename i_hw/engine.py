from player import Player
import random
import time
import threading

class Engine:

	UP = "UP"
	LEFT = "LEFT"
	RIGHT = "RIGHT"
	DOWN = "DOWN"

	def __init__(self):
		self.dummy_player = Player("DUMMY", Player.DUMMY)


	def add_player(self, game_field, player):
		game_field.append_spectators(player)
		game_field.append_player(player)

	def change_player_role(self, game_field, requested_type, player):
		if requested_type == 1: # client wants to be a spec again
			self.remove_player_from_board(game_field, player)
		elif(player.is_spectator() and self.register_player(game_field, player)):
			player.set_player_type(requested_type)
		return "Player " + player.name + " is now type " + str(requested_type)

	def remove_player_from_board(self, game_field, player):
		coordinates = player.get_location()
		game_field.set_on_game_board(coordinates[0], coordinates[1], self.dummy_player)
		game_field.append_spectators(player)
		player.kill()

	def register_players(self, game_field):
		for i in game_field.spectators:
			if(not i.is_spectator()):
				if(len(game_field.get_all_players()) < game_field.get_size()):
					self.register_player(game_field, i)
		game_field.clear_spectators()

	def register_player(self, game_field, player):
		notPlaced = True
		size = game_field.get_size()
		m = size[0]
		n = size[1]
		while notPlaced:
			x = random.randint(0, n-1)
			y = random.randint(0, m-1)
			prev_player = game_field.get_from_game_board(x,y) 
			if(prev_player.get_icon() == '_'):
				game_field.set_on_game_board(x,y,player)
				player.set_location(x,y)
				notPlaced = False
		return not notPlaced

	def process_moves(self, game_field):
		for uuid, move in game_field.get_moves().iteritems():
			if(move[0] == ""):
				continue
			player = game_field.get_player(uuid)
			self.move_player(game_field, player, move[0], move[1])
			game_field.request_move(uuid, "", -1)

	def move_player(self, game_field, player, direction, times = 1):
		moved = False
		if self.valid_move_for_character(player, times):
			new_coordinates = self.get_new_coordinates(player, direction, times)
			if self.coordinates_are_valid(game_field, new_coordinates):
				current_location = player.get_location()
				old_player = game_field.get_from_game_board(new_coordinates[0], new_coordinates[1])

				if(old_player.is_dummy() or (old_player.is_fly() and player.is_frog())):
					game_field.set_on_game_board(new_coordinates[0], new_coordinates[1], player)
					game_field.set_on_game_board(current_location[0], current_location[1], self.dummy_player)
					player.set_location(new_coordinates[0], new_coordinates[1])
					moved = True
				if(old_player.is_fly() and player.is_frog()):
					player.inc_points()
					self.remove_player_from_board(game_field, old_player)
					game_field.set_on_game_board(new_coordinates[0], new_coordinates[1], player)
		return moved

	def coordinates_are_valid(self, game_field, new_coordinates):
		x = new_coordinates[0]
		y = new_coordinates[1]

		field_size = game_field.get_size()
		return x >= 0 and x < field_size[1] and y >= 0 and y < field_size[0]

	def valid_move_for_character(self, player, times):
		return times == 2 and player.is_frog() or times == 1

	def get_new_coordinates(self, player, direction, times):
		coordinates = player.get_location()
		current_x = coordinates[0]
		current_y = coordinates[1]
		new_x = current_x
		new_y = current_y
		if(direction == Engine.UP):
			new_y = current_y - times
		elif(direction == Engine.DOWN):
			new_y = current_y + times
		elif(direction == Engine.LEFT):
			new_x = current_x - times
		elif(direction == Engine.RIGHT):
			new_x = current_x + times
		return (new_x, new_y)
		

	def track_time(self, game_field):
		for uuid,player in game_field.get_all_players().iteritems():
			time_limit_exceeded = player.last_action_limit_exceeded()
			if(time_limit_exceeded):
				player.update_last_scored()
				if(player.is_frog()):
					self.remove_player_from_board(game_field, player)
				elif(player.is_fly()):
					player.inc_points()

	def request_move(self, game_field, uuid, direction, times):
		game_field.request_move(uuid, direction, times)

	# FOR GUI
	def board_shading(self, game_field, special_board):
		line = []
		size = game_field.get_size()
		for x in range (size[1]):
			line.append('#')
		
		for y in range (size[0]):
			special_board.append(line[:])
		
	def get_visible_area(self, game_field, player):
		coordinates = player.get_location()
		special_board = []
		self.board_shading(game_field, special_board)
		size = game_field.get_size()
		m = size[0]
		n = size[1]
		if (player.is_spectator()):
			for x in range(m):
				for y in range(n):
					p = game_field.get_from_game_board(y,x)
					special_board[x][y] = p.get_icon()
		elif (player.is_frog()): 
			for y in range(-1,2):
				for x in range(-1,2):
					if(x != 0 and y != 0):
						continue
					if (self.coordinates_are_valid(game_field, [coordinates[0]+x, coordinates[1] + y])): # make sure we are editing valid area
						p = game_field.get_from_game_board(coordinates[0]+x, coordinates[1]+y)
						special_board[coordinates[1] + y][coordinates[0] + x] = p.get_icon()
		elif (player.is_fly()):
			for y in range(-2,3):
				for x in range(-2, 3):
					if(x != 0 and y != 0):
						continue
					if (self.coordinates_are_valid(game_field, [coordinates[0]+x, coordinates[1] + y])):
						p = game_field.get_from_game_board(coordinates[0]+x, coordinates[1]+y)
						special_board[coordinates[1] + y][coordinates[0] + x] = p.get_icon()
		return special_board

	def valid_player_type(self, player_type):
		return player_type == Player.SPECTATOR or player_type == Player.FROG or player_type == Player.FLY

	def run(self, game_field):
		while(True):
			time.sleep(0.5)
			self.register_players(game_field)
			self.track_time(game_field)
			self.process_moves(game_field)		
	
